import turtle as t
import random
t.Screen()
def randomcolor():
    colors = ['red','orange','yellow','green','cyan','blue','violet']
    print(random.choice(colors))
def triangle(color,size):

    for i in range(3):  #Loop through three times to draw a triangle
        t.color(color)
        t.forward(size)
        t.right(120)
    t.done()
def star(color,size):

    for i in range(5):  #Loop through five times to draw a star
        t.color(color)  
        t.forward(size)
        t.right(144)
    t.done()

def square(color,size):

    for i in range(4):  #Loop through four times to draw a square
        t.color(color)
        t.forward(size)
        t.right(90)
    t.done()
def pentagon(color,size):

    for i in range(5): #Loop through five times to draw a pentagon
        t.color(color)
        t.forward(size)
        t.right(72)
    t.done()

t.shape("turtle")
#draw

print("What shape do you want to draw? Triangle, Star, Square, or Pentagon.")

print("Type the name of the shape")
#What shape does the user want to draw?
shape1 = input().lower()
print("What color do you want to fill? Or you want a random color.")
print("Type r for random color or type the color you want.")
yesorno = input().lower()
if yesorno 
color1 = input().lower()
print("How big do you want the shape to be? Small, Medium, or Large.")
size1 = input().lower()

if size1 == "small":
      size1 = 20
if size1 == "medium":
      size1 = 50
if size1 == "large":
      size1 = 100

if shape1 == ("triangle"):

    triangle(color1,size1)

elif shape1 == ("star"):

    star(color1,size1)

elif shape1 == ("square"):

    square(color1,size1)

elif shape1 == ("pentagon"):

    pentagon(color1,size1)

