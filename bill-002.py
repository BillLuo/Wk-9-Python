#Bill Luo 
import turtle as t
import random
t.Screen()  #Create a graphics window
def triangle(color,size):

    for i in range(3):  #Loop through three times to draw a triangle
        t.color(color)  #Use built-in function turtle.color to change the color of the pen.
        t.forward(size) #Tell t to move forward by what units
        t.right(120)    #Turn by 120 degrees
    t.done()
    
def star(color,size):

    for i in range(5):  #Loop through five times to draw a star
        t.color(color)  
        t.forward(size) 
        t.right(144)    #Turn by 144 degrees
    t.done()

def square(color,size):

    for i in range(4):  #Loop through four times to draw a square
        t.color(color)
        t.forward(size)
        t.right(90)     #Turn by 90 degrees
    t.done()
    
def pentagon(color,size):

    for i in range(5): #Loop through five times to draw a pentagon
        t.color(color)
        t.forward(size)
        t.right(72)     #Turn by 72 degrees
    t.done()

t.shape("turtle")   #Draw
print("What shape do you want to draw? Triangle, Star, Square, or Pentagon.")
print("Type the name of the shape") #What shape does the user want to draw?
shape1 = input().lower()
print("Do you want a random color? Type y for yes and n for no.")   #Use a random color or a specific color
check = input().lower() #Check whether user wants a random color or a specific number
if check == 'y': #Generate a random color
    colors = ['red','orange','yellow','green','cyan','blue','violet']
    rcolor = random.choice(colors)
    print (rcolor)
    color1 = rcolor
elif check == 'n':
    print("What color do you like for your pen? Type the color.")
    color1 = input().lower() #User chooses a color
print("How big do you want the shape to be? Small, Medium, or Large.")
size1 = input().lower() #Let user choose a size

if size1 == "small": #Different size options
      size1 = 20
if size1 == "medium":
      size1 = 50
if size1 == "large":
      size1 = 100

if shape1 == ("triangle"): #Check the user input

    triangle(color1,size1)

elif shape1 == ("star"):

    star(color1,size1)

elif shape1 == ("square"):

    square(color1,size1)

elif shape1 == ("pentagon"):

    pentagon(color1,size1)

